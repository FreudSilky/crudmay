import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Edit from './views/edit';
import Create from './views/create';
import Pdf from './views/pdf'
var hist = createBrowserHistory();
ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route exact path='/' component={App} />
      <Route path='/pdfPerson/:id' component={Pdf} />
      <Route path='/edit/:id' component={Edit} />
      <Route path='/create' component={Create} />
    </Switch>
  </Router>,
  document.getElementById('root')
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
