import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

const config = {
  apiKey: "AIzaSyC5rQPhUAtuWFX2SP8yCV8NAf5oOOBnz7Q",
  authDomain: "crudreact-8c515.firebaseapp.com",
  databaseURL: "https://crudreact-8c515.firebaseio.com",
  projectId: "crudreact-8c515",
  storageBucket: "crudreact-8c515.appspot.com",
  messagingSenderId: "770376206193",
  appId: "1:770376206193:web:fc06ffcd563738d71ad8d0",
  measurementId: "G-425K5Y3EZ4"
};
 

export default firebase.initializeApp(config);
