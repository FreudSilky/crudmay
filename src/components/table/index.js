import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import { withStyles } from '@material-ui/core/styles';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import firebase from '../../utils/Firebase';
import Preloader from '../../components/PreLoader';
import Container from '@material-ui/core/Container';

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

export default class MaterialTableDemo extends Component {
    _isMoment = false;

    constructor(props) {
        super(props);
        this.state = {
            empleado: [],
            open: false,
            id: "",
            comit: false,
        };
    }

    handleClickOpen = (id) => {
        this.setState({
            open: true,
            id,
        });
    };

    handleClose = () => {
        this.setState({
            open: false,
            id: "",
        });
    };
    onCollectionUpdate = () => {

        firebase.database().ref('empleado/').once('value', snapshot => {
            let empleado = [];
            snapshot.forEach(row => {
                empleado.push({
                    id: row.key,
                    nombre: row.val().nombre,
                    aPaterno: row.val().aPaterno,
                    aMaterno: row.val().aMaterno,
                    dateNacimiento: row.val().dateNacimiento,
                })
            });
            if (this._isMoment) {
                this.setState({
                    empleado,
                    comit: true,
                });
            }

        })
    }
    delete = () => {
        const ref = firebase.database().ref().child(`empleado/${this.state.id}`);
        ref.remove().then(() => {
            this.setState({
                open: false,
                id: "",
            });
            this.onCollectionUpdate();
        }).catch(function (error) {
        });
    }
    dilog() {
        return (
            <Container>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Atencion"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            ¿Desea eliminar?
            </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            No
            </Button>
                        <Button onClick={this.delete} color="primary" autoFocus>
                            Si
            </Button>
                    </DialogActions>
                </Dialog>
            </Container>
        )
    };
    componentDidMount() {
        this.onCollectionUpdate();
    }
    componentWillMount() {
        this._isMoment = true;
    }

    render() {
        if (!this.state.comit) {
            return (
                <Preloader></Preloader>
            )
        }
        if (this.state.empleado !== null) {
            return (
                <Container>
                    {this.dilog()}
                    <Table aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="right">Nombre</StyledTableCell>
                                <StyledTableCell align="right">A.Paterno</StyledTableCell>
                                <StyledTableCell align="right">A.Materno</StyledTableCell>
                                <StyledTableCell align="right">F.Nacimiento</StyledTableCell>
                                <StyledTableCell align="right">Editar</StyledTableCell>
                                <StyledTableCell align="right">PDF</StyledTableCell>
                                <StyledTableCell align="right">Eliminar</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.empleado.map(row => (
                                <StyledTableRow key={row.id}>
                                    <StyledTableCell align="right">{row.nombre}</StyledTableCell>
                                    <StyledTableCell align="right">{row.aPaterno}</StyledTableCell>
                                    <StyledTableCell align="right">{row.aMaterno}</StyledTableCell>
                                    <StyledTableCell align="right">{row.dateNacimiento}</StyledTableCell>
                                    <StyledTableCell align="right">
                                        <Link to={`/edit/${row.id}`} style={{ marginLeft: 10 }}>
                                            <Button variant="contained">
                                                Editar
                                         </Button>
                                        </Link>
                                    </StyledTableCell>
                                    <StyledTableCell align="right"><Link to={`/pdfPerson/${row.id}`} target="_blank" style={{ marginLeft: 10 }}>
                                        <Button variant="contained" color="primary">
                                            PDF
                                            </Button>
                                    </Link>
                                    </StyledTableCell>
                                    <StyledTableCell align="right">
                                        <Button variant="contained" color="secondary" onClick={() => this.setState({
                                            open: true,
                                            id: row.id,
                                        })}
                                        >
                                            Eliminar
                                         </Button>
                                    </StyledTableCell>
                                </StyledTableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Container>)
        }
    }
}
