import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';



const ColorLinearProgress = withStyles({
  colorPrimary: {
    backgroundColor: 'blue',
  },
  barColorPrimary: {
    backgroundColor: 'white',
  },
})(LinearProgress);



const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function CustomizedProgressBars() {
  const classes = useStyles();

  return (
    <div className={classes.root} style={{textAlign:'center'}}>
      <ColorLinearProgress className={classes.margin} style={{height:10}} />
    </div>
  );
}