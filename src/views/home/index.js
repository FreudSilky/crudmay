import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Table from '../../components/table';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
class App extends Component {
  componentDidMount(){
  }
    render() {
    return (
      <Container>
        <Container style={{ marginBottom: 20,marginTop:10}}>
          <Typography variant="h3" gutterBottom>
            Empleados
      </Typography>
          <Link to="/create" ><Button variant="contained" color="primary">
            Agregar
              </Button></Link>
        </Container>
        <Table></Table>
      </Container>
    );
  }
}

export default App;
