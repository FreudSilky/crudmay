import React, { Component } from 'react';
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'colum',
    backgroundColor: 'white'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
});

class MyDocument extends Component {
 datos(){
  const {empleado} =this.props;
  return(
    empleado.map(element => 
      <View style={styles.section} key={element.id}>
      <Text>Nombre: {element.nombre}</Text>
      <Text>Apellido paterno: {element.aPaterno}</Text>
      <Text>Apellido materno: {element.aMaterno}</Text>
      <Text>Fecha de nacimiento: {element.dateNacimiento}</Text>
      </View>
    )
  )
 }
componentDidMount(){

}
  
  render() {
      return(
        <Document>
        <Page size="A4" style={styles.page}>
            {this.datos()}
        </Page>
      </Document>
      )}

}


export default MyDocument;
