import React, { Component } from 'react';
import { PDFViewer } from '@react-pdf/renderer';
import Mydocument from './Document';
import firebase from '../../utils/Firebase';
import Preloader from '../../components/PreLoader';
import Container from '@material-ui/core/Container';

class Pdf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      empleado: [],
      comit: false,
    };
  }
  consulta() {
    const { id } = this.props.match.params;
    firebase.database().ref('empleado/' + id).once('value', snapshot => {
      let empleado=[{
        id: snapshot.key,
        nombre: snapshot.val().nombre,
        aPaterno: snapshot.val().aPaterno,
        aMaterno: snapshot.val().aMaterno,
        dateNacimiento: snapshot.val().dateNacimiento,
      }]
      this.setState({
        empleado,
        comit: true,
      });
      console.log(this.state);
    })

  }

  componentDidMount() {
    this.consulta();
  }
  render() {
    if (!this.state.comit) {
      return (
        <Container>
          <Preloader></Preloader>
        </Container>
      )
    }
    if (this.state.empleado !== null)
      return (
        <Container >
          <PDFViewer style={{ height: 700, width: '100%', padding: 0, margin: 0 }} >
            <Mydocument empleado={this.state.empleado} />
          </PDFViewer>
        </Container>
      )
  }
}

export default Pdf;