import React, { Component } from 'react';
import firebase from '../../utils/Firebase';
import { Link } from 'react-router-dom';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
class Create extends Component {

  constructor() {
    super();
    this.ref = firebase.database().ref();
    this.state = {
      formData: {
        nombre: '',
        aPaterno: '',
        aMaterno: '',
        dateNacimiento: "2017-05-24",
      },
    }
  }
  handleChange = (event) => {
    const { formData } = this.state;
    formData[event.target.name] = event.target.value;
    this.setState({ formData });
  }
  componentDidMount() {
    
  }
  handleSubmit = (e) => {
    e.preventDefault();

    let data = {};
    const key = this.ref.child('empleado').push().key;
    data[`empleado/${key}`] = this.state.formData;
    this.ref.update(data).then(() => {
      this.setState({
        formData: {
          nombre: '',
          aPaterno: '',
          aMaterno: '',
          dateNacimiento: '',
        },
      });
      this.props.history.push("/")
    }).catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    const { formData } = this.state;
    return (
      <Container style={{ textAlign: 'center' }}>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
        >
          <Typography variant="h3" gutterBottom>
            Agregar
      </Typography>
          <TextValidator
            style={{ width: '60%' }}
            label="Nombre"
            inputProps={{
              maxLength: 15,
            }}
            onChange={this.handleChange}
            name="nombre"
            value={formData.nombre}
            validators={['required', 'matchRegexp:[a-zA-Z]$']}
            errorMessages={['Este campo es requerido', 'solo letras']}
          />
          <br />
          <TextValidator
            style={{ width: '60%' }}
            label="Apellido paterno"
            inputProps={{
              maxLength: 15,
            }}
            onChange={this.handleChange}
            name="aPaterno"
            value={formData.aPaterno}
            validators={['required', 'matchRegexp:[a-zA-Z]$']}
            errorMessages={['Este campo es requerido', 'solo letras']}
          />
          <br />
          <TextValidator
            style={{ width: '60%' }}
            label="Apellido materno"
            inputProps={{
              maxLength: 15,
            }}
            onChange={this.handleChange}
            name="aMaterno"
            value={formData.aMaterno}
            validators={['required', 'matchRegexp:[a-zA-Z]$']}
            errorMessages={['Este campo es requerido', 'solo letras']}
          />
          <br />
          <TextValidator
            style={{ width: '60%' }}
            label="Fecha de nacimiento"
            onChange={this.handleChange}
            name="dateNacimiento"
            value={formData.dateNacimiento}
            validators={['required',]}
            errorMessages={['Este campo es requerido',]}
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <br />
          <Button
            style={{ margin: 10 }}
            variant="contained" color="primary"
            type="submit"
          >
            Agregar
          </Button>
          <Link to="/" ><Button variant="contained" color="secondary">
            Cancelar
          </Button></Link>
        </ValidatorForm>
      </Container >
    );
  }
}

export default Create;
