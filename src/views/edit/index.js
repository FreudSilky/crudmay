import React, { Component } from 'react';
import firebase from '../../utils/Firebase';
import { Link } from 'react-router-dom';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography';
import Preloader from '../../components/PreLoader';
import axios from 'axios';

class Edit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      formData: {},
      comit: false,
    };
  }
  consulta() {
    try {
      const { id } = this.props.match.params;
      firebase.database().ref('empleado/' + id).once('value', snapshot => {
          let formData = {

            id: snapshot.key,
            nombre: snapshot.val().nombre,
            aPaterno: snapshot.val().aPaterno,
            aMaterno: snapshot.val().aMaterno,
            dateNacimiento: snapshot.val().dateNacimiento,

          }
          this.setState({
            formData,
            comit: true,
          });

      })

    } catch (error) {
      const name = "ron";
      const email = "rodrigomentado@gmail.com";
      const message = "ron";
      axios({
        method: "POST",
        url: "https://app-freud.herokuapp.com/send",
        data: {
          name: name,
          email: email,
          messsage: message
        }
      })
    }

  }

  componentDidMount() {
    this.consulta();
  }

  handleChange = (event) => {
    const { formData } = this.state;
    formData[event.target.name] = event.target.value;
    this.setState({ formData });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const validate = this.state.formData;
    let data = Object.assign({}, validate);
    const ref = firebase.database().ref().child(`empleado/${this.state.formData.id}`);
    ref.update(data).then(() => {
      this.setState({
        data: {}
      });
      this.props.history.push("/");
    })
  }
  render() {
    try {
      if (!this.state.comit) {
        return (
          <Container>
            <Preloader></Preloader>
          </Container>
        )
      }
      const { formData } = this.state;
      if (formData !== null) {
        return (
          <Container style={{ textAlign: 'center', marginBottom: 20, marginTop: 10 }}>
            <ValidatorForm
              ref="form"
              onSubmit={this.handleSubmit}
            >
              <Typography variant="h3" gutterBottom>
                Editar
      </Typography>
              <TextValidator
                style={{ width: '60%' }}
                label="Nombre"
                inputProps={{
                  maxLength: 15,
                }}
                onChange={this.handleChange}
                name="nombre"
                maxlength='30'
                value={`${formData.nombre}`}
                validators={['required', 'matchRegexp:[a-zA-Z]$']}
                errorMessages={['Este campo es requerido', 'solo letras']}
              />
              <br />
              <TextValidator
                style={{ width: '60%' }}
                label="Apellido paterno"
                inputProps={{
                  maxLength: 15,
                }}
                onChange={this.handleChange}
                name="aPaterno"
                value={`${formData.aPaterno}`}
                validators={['required', 'matchRegexp:[a-zA-Z]$']}
                errorMessages={['Este campo es requerido', 'solo letras']}
              />
              <br />
              <TextValidator
                style={{ width: '60%' }}
                label="Apellido materno"
                inputProps={{
                  maxLength: 15,
                }}
                onChange={this.handleChange}
                name="aMaterno"
                value={`${formData.aMaterno}`}
                validators={['required', 'matchRegexp:[a-zA-Z]$']}
                errorMessages={['Este campo es requerido', 'solo letras']}
              />
              <br />
              <TextValidator
                style={{ width: '60%' }}
                label="Fecha de nacimiento"
                onChange={this.handleChange}
                name="dateNacimiento"
                value={`${formData.dateNacimiento}`}
                validators={['required',]}
                errorMessages={['Este campo es requerido',]}
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <br />
              <Button
            style={{ margin: 10 }}
            variant="contained" color="primary"
            type="submit"
          >
            Agregar
          </Button>
          <Link to="/" ><Button variant="contained" color="secondary">
            Cancelar
          </Button></Link>
            </ValidatorForm>
          </Container>
        );
      }
    } catch (err) {
      console.log("entro");
      const name = "ron";
      const email = "rodrigomentado@gmail.com";
      const message = "ron";
      axios({
        method: "POST",
        url: "https://app-freud.herokuapp.com/send",
        data: {
          name: name,
          email: email,
          messsage: message
        }
      })
    }
  }
}

export default Edit;
